angular.module('uk-toolkit.components')
    .directive('ukPhoneInput', function($filter, $Localization) {
        return {
            restrict: 'E',
            require: ['ngModel'],
            scope: {
                ngModel: '=', // Ng-Model
                placeholder: '@',
                name:'@',
                country: '@',
                errorResource: '@'
            },
            transclude: false,
            templateUrl: 'uk-phone-input/uk-phone-input.tpl.html',
            link: function(scope, elm, attrs, ctrls) {
                var ctrl = ctrls[0];
                ctrl.$validators.text = function(modelValue, viewValue) {
                    // because we always set null when model is invalid :P!
                    // easy cake!.
                    return modelValue !== null;
                };
            },
            controller: function($scope, $element, $q) {
                // ------------------------------
                // Model && default values
                // ------------------------------
                var focusActiveClass = "uk-focus-active";
                var vm = $scope.vm = {
                    // Validation Flag
                    isValidityValid: true,
                    phoneLength: 9
                };
                $scope.format = $scope.format !== "false" ? true : false;

                //Check if the resource exists!
                var locale = $Localization.exists($scope.errorResource);
                if (locale) {
                    vm.errorLabel = $Localization.get($scope.errorResource);
                }

                // ------------------------------
                // Private Method's
                // ------------------------------
                var isPhoneOrOfuscatePhone = function(raw) {
                    //VALIDATE EMAIL
                    var value = (raw || '');
                    if (value.length === 0) {
                        return true;
                    }
                    return (/^[0-9-*]+$/.test(value));
                };

                var writeValue = function(value) {
                    if (value !== null && value !== undefined) {
                        var cleanPhone = (value + "").replace(/\ /ig, '');
                        var _isPhone = isPhoneOrOfuscatePhone(cleanPhone);
                        var fullyValid = false;
                        var phoneValue = cleanPhone;

                        if (_isPhone) {

                            var minLength = 9;
                            var maxLength = 9;
                            var isInMinLength = true;
                            var isInMaxLength = true;

                            if (cleanPhone.toString().length < minLength) {
                                isInMinLength = false;
                            }

                            if (cleanPhone.toString().length > maxLength) {
                                isInMaxLength = false;
                            }

                            if (isInMinLength && isInMaxLength) {
                                fullyValid = true; // All validation pass!
                            }
                        }

                        vm.isValidityValid = fullyValid;

                        $scope.ngModel = vm.isValidityValid ? phoneValue : null;
                    } else {
                      $scope.ngModel = null;
                      if (value === undefined) {
                        vm.viewValue = null;
                      }
                    }
                };

                // ------------------------------
                // Exposed Method's
                // ------------------------------
                vm.isValid = function() {
                    return vm.isValidityValid;
                };

                vm.isFilled = function() {
                    var value = (vm.viewValue || '');
                    return value.toString().length > 0;
                };

                //Check character input is only number
                characterNumber = function(evt) {
                    evt = (evt) ? evt : window.event;
                    var charCode = (evt.which) ? evt.which : evt.keyCode;
                    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                        return false;
                    }
                    return true;
                };

                // ------------------------------
                // Event's
                // ------------------------------
                vm.onKeyUp = function(ev, thisObject) {
                    switch (ev.keyCode) {
                        case 187: //*
                            if (thisObject.vm.viewValue.length < 2) {
                                thisObject.vm.viewValue = "";
                            } else {
                                thisObject.vm.viewValue = thisObject.vm.viewValue.substring(0, thisObject.vm.viewValue.length - 1);
                                break;
                            }
                    }
                };

                vm.onFocus = function() {
                    vm.phoneLength = 9;
                    $element.addClass(focusActiveClass);
                    if (vm.isValidityValid) {
                        vm.viewValue = $scope.ngModel;
                    }
                };
                vm.onBlur = function() {
                    vm.phoneLength = 11;
                    $element.removeClass(focusActiveClass);
                    if (vm.isValidityValid && $scope.ngModel) {
                        vm.viewValue = $scope.ngModel.toString().substring(0, 1) + ' ' + $scope.ngModel.toString().substring(1, 5) + ' ' + $scope.ngModel.toString().substring(5, 9);
                    }
                };
                vm.onChange = writeValue;

                $scope.$watch("ngModel", function(value) {
                    writeValue(value);

                    if (!vm.isValidityValid && value) {
                        vm.viewValue = value;
                    } else {
                        //Only "format" when user is not edite the control
                        if (!$element.hasClass(focusActiveClass)) {
                            vm.onBlur();
                        }
                    }

                });
            }
        };
    });
